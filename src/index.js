import Store from './store'
import Order from './order'
import Cart from './cart'

const shopiker = {
  Store,
  Order,
  Cart,
}

export default shopiker
