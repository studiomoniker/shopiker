import formatPrice from './utils/formatPrice'

class Product {
  constructor(options) {
    if (options.id === undefined) throw new Error('id is undefined')
    if (options.title === undefined) throw new Error('title is undefined')
    if (options.subtitle === undefined) throw new Error('subtitle is undefined')
    if (options.taxRate === undefined) throw new Error('taxRate is undefined')
    if (options.stock === undefined) throw new Error('stock is undefined')
    if (options.manageStock === undefined)
      throw new Error('manageStock is undefined')
    if (options.allowBackorder === undefined)
      throw new Error('allowBackorder is undefined')
    if (options.status === undefined) throw new Error('status is undefined')
    if (options.type === undefined) throw new Error('type is undefined')
    if (options.description === undefined)
      throw new Error('description is undefined')
    if (options.price === undefined) throw new Error('price is undefined')

    if (
      !('amount' in options.price) ||
      !('currency' in options.price) ||
      !('includesTax' in options.price)
    )
      throw new Error('Price object is not valid')
    this.locale = options.locale
    this.id = options.id
    this.title = options.title
    this.subtitle = options.subtitle
    this.taxRate = options.taxRate
    this.stock = options.stock
    this.manageStock = options.manageStock
    this.allowBackorder = options.allowBackorder
    this.status = options.status
    this.type = options.type
    this.description = options.description
    this.price = options.price
    this.parent = options.parent || undefined

    // ADD META DATA, EG EVERYTHING EXCEPT REQUIRED DATA
    if ('meta' in options) this.meta = options.meta
    const productKeys = [
      'amount',
      'amountFormatted',
      'isVariation',
      'locale',
      'parent',
      'id',
      'title',
      'subtitle',
      'taxRate',
      'stock',
      'manageStock',
      'allowBackorder',
      'status',
      'type',
      'images',
      'description',
      'price',
    ]
    Object.keys(options).forEach(key => {
      if (key === 'meta' || productKeys.includes(key)) return
      if (!('meta' in this)) this.meta = {}
      this.meta[key] = options[key]
    })
  }

  get isVariation() {
    return !!this.parent
  }

  isInStock(quantity) {
    if (!this.manageStock || this.allowBackorder) return true
    if (this.stock >= quantity) return true
    return false
  }

  get amount() {
    if (this.price.includesTax) {
      return this.price.amount
    }
    return this.price.amount * (1 + this.taxRate / 100)
  }

  get amountFormatted() {
    return formatPrice(this.locale, this.amount, this.price.currency)
  }
}

export default Product
