class ShippingMethod {
  constructor(options) {
    this.id = options.id || 'standard'
    this.isDefault = options.isDefault
    this.name = options.name
    this.zones = options.zones
    this.includesTax = options.includesTax
    this.currency = options.currency
    this.description = options.description
    this.taxRate = options.taxRate
  }
}

export default ShippingMethod
