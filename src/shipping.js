/* eslint-disable no-console */

import mitt from 'mitt'
import { getCountry } from './utils/ip'
import ShippingMethod from './shippingMethod'
import formatPrice from './utils/formatPrice'

class Shipping {
  constructor({
    method = undefined,
    country = {
      alpha2code: '',
      name: '',
    },
    items,
    locale,
  } = {}) {
    if (method && !(method instanceof ShippingMethod)) method = new ShippingMethod(method)
    this.locale = locale
    this.items = items
    this.country = country
    this.method = method
    const mitter = mitt()
    this.on = mitter.on
    this.emit = mitter.emit
    this.off = mitter.off
    if (this.country.alpha2code === '' || this.country.name === '') {
      this.setCountryByIp()
    }
  }

  get amount() {
    if (!this.zone || this.items.length === 0 || !this.needsShipping) return 0

    if (this.method.includesTax) {
      return this.zone.amount
    }
    const amount = this.zone.amount * (1 + (this.method.taxRate / 100))
    return amount
  }

  get needsShipping() {
    return this.items.reduce((acc, cur) => {
      if (cur.product.type === 'physical') acc = true
      return acc
    }, false)
  }

  get amountFormatted() {
    if (!this.method || !this.zone || !this.needsShipping) return '-'
    const { currency } = this.method
    if (this.amount === 0 || this.items.length === 0) return formatPrice(this.locale, 0, currency)
    return formatPrice(this.locale, this.amount, currency)
  }

  get zone() {
    if (!this.method) return undefined

    if (this.country.alpha2code !== '' && this.country.name !== '') {
      const zone = this.method.zones.find((z) => {
        if (z.countries.includes(this.country.alpha2code)) return true
        return false
      })
      if (zone) return zone
    }
    const zone = this.method.zones.reduce((acc, cur) => {
      if (cur.amount > acc.amount) {
        acc = cur
      }
      return acc
    }, {
      ...this.method.zones[0],
    })
    return zone
  }

  setCountry(country) {
    if (this.country.alpha2code !== country.alpha2code) {
      this.country = country
      this.emit('country-changed')
      this.emit('updated')
    } else {
      console.warn('did not update shipping country because country is already set')
    }
  }

  setMethod({ method }) {
    if (this.method) {
      console.warn('did not update shipping method because method is already set')
      return
    }
    this.method = method
    this.emit('method-changed')
    this.emit('updated')
  }

  async setCountryByIp() {
    this.country = await getCountry()
  }
}

export default Shipping
