class Promotion {
  constructor({ type, key, amount }) {
    this.type = type
    this.key = key
    this.amount = amount
  }
}

export default Promotion
