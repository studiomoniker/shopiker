import Order from './order'
import Cart from './cart'
import Product from './product'
import ShippingMethod from './shippingMethod'
import formatPrice from './utils/formatPrice'

class Store {
  constructor({
    cartStorage = 'cart',
    orderStorage = 'order',
    addressesStorage = 'addresses',
    currency = 'EUR',
    locale = 'en-US',
  } = {}) {
    this.currency = currency
    this.locale = locale

    this.cart = new Cart({ cartStorage, currency, locale })
    this.order = new Order({
      cart: this.cart,
      orderStorage,
      addressesStorage,
    })
    this.shippingMethods = []
  }

  setShippingMethods(shippingMethods) {
    this.shippingMethods = shippingMethods.map(data => new ShippingMethod(data))
    this.setDefaultShippingMethod()
  }

  setDefaultShippingMethod() {
    const method = this.shippingMethods.find(
      shippingMethod => shippingMethod.isDefault,
    )
    if (method) {
      this.cart.shipping.setMethod({
        method,
      })
    }
  }

  createProduct(data) {
    data.locale = this.locale
    return new Product(data)
  }

  clear() {
    this.cart.clear()
    this.order.clear()
    this.setDefaultShippingMethod()
    this.cart.shipping.setCountryByIp()
  }

  formatPrice(amount) {
    return formatPrice(this.locale, amount, this.currency)
  }
}

export default Store
