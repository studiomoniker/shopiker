import mitt from 'mitt'

class Order {
  constructor({
    cart,
    customerId,
    orderStorage = 'order',
    addressesStorage = 'addresses',
  }) {
    if (orderStorage && localStorage.getItem(orderStorage)) {
      const orderData = JSON.parse(localStorage.getItem(orderStorage))
      this.id = orderData.id
      this.customerId = orderData.customerId
      this.createdAt = orderData.createdAt
      this.updatedAt = orderData.updatedAt
    } else {
      // this.id = Math.random().toString(16).slice(2)
      this.customerId = customerId
      this.createdAt = new Date().valueOf()
      this.updatedAt = new Date().valueOf()
    }

    if (addressesStorage && sessionStorage.getItem(addressesStorage)) {
      const addressesData = JSON.parse(sessionStorage.getItem(addressesStorage))
      if ('billing' in addressesData) {
        this.billing = addressesData.billing
      }
      if ('shipping' in addressesData) {
        this.shipping = addressesData.shipping
      }
    } else {
      this.billing = undefined
      this.shipping = undefined
    }

    this.orderStorage = orderStorage
    this.addressesStorage = addressesStorage
    this.cart = cart
    const mitter = mitt()
    this.on = mitter.on
    this.off = mitter.off
    this.emit = mitter.emit
    this.save()
  }

  toJson() {
    const order = JSON.parse(JSON.stringify(this))
    order.cart.shipping.amount = parseInt(this.cart.shipping.amount, 10)
    order.cart.shipping.needsShipping = this.cart.shipping.needsShipping
    order.cart.shipping.amountFormatted = this.cart.shipping.amountFormatted
    order.cart.shipping.zone = this.cart.shipping.zone
    order.cart.items = this.cart.items.map(item => {
      const i = JSON.parse(JSON.stringify(item))
      i.amount = parseInt(item.amount, 10)
      i.amountFormatted = item.amountFormatted
      i.product.isVariation = item.product.isVariation
      i.product.amount = parseInt(item.product.amount, 10)
      i.product.amountFormatted = item.product.amountFormatted
      return i
    })
    order.separateShippingAddress = this.separateShippingAddress
    order.hasAddress = this.hasAddress
    return order
  }

  get separateShippingAddress() {
    return !!this.shipping
  }

  get hasAddress() {
    return !!this.billing || !!this.shipping
  }

  save() {
    this.emit('updated')
    this.updatedAt = new Date().valueOf()
    const d = {
      id: this.id,
      customerId: this.customerId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    }
    localStorage.setItem(this.orderStorage, JSON.stringify(d))

    const a = {
      billing: this.billing,
      shipping: this.shipping,
    }
    sessionStorage.setItem(this.addressesStorage, JSON.stringify(a))
  }

  setAddresses({ billing, shipping }) {
    this.shipping = shipping
    this.billing = billing
    this.assignShippingAddress()
    this.save()
  }

  assignShippingAddress() {
    if (
      this.shipping &&
      this.separateShippingAddress &&
      'country' in this.shipping &&
      'alpha2code' in this.shipping.country &&
      this.shipping.country.alpha2code.length === 2
    ) {
      this.cart.shipping.setCountry(this.shipping.country)
    } else if (this.separateShippingAddress) {
      this.cart.shipping.setCountryByIp()
    } else if (
      !this.separateShippingAddress &&
      this.billing &&
      'country' in this.billing &&
      'alpha2code' in this.billing.country &&
      this.billing.country.alpha2code.length === 2
    ) {
      this.cart.shipping.setCountry(this.billing.country)
    } else {
      this.cart.shipping.setCountryByIp()
    }
  }

  clear() {
    // this.id = Math.random().toString(16).slice(2)
    this.id = undefined
    this.createdAt = new Date().valueOf()
    this.updatedAt = new Date().valueOf()
    this.customerId = undefined
    this.billing = undefined
    this.shipping = undefined

    this.save()
  }

  clearCustomerId() {
    this.customerId = undefined
    this.save()
  }

  setCustomerId(id) {
    this.customerId = id
    this.save()
  }
}

export default Order
