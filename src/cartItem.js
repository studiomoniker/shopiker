import Product from './product'
import formatPrice from './utils/formatPrice'
import mitt from 'mitt'

class CartItem {
  constructor({ locale, quantity = 0, product = undefined } = {}) {
    this.product = product instanceof Product ? product : new Product(product)
    this.locale = locale
    this.id = product.id
    this.quantity = quantity
    const mitter = mitt()
    this.on = mitter.on
    this.off = mitter.off
    this.emit = mitter.emit
  }

  get amount() {
    return this.product.amount * this.quantity
  }

  get amountFormatted() {
    return formatPrice(this.locale, this.amount, this.product.price.currency)
  }

  increment(quantity = 1) {
    if (quantity < 0) {
      this.decrement(-quantity)
    } else if (
      !this.product.manageStock ||
      this.product.allowBackorder ||
      this.product.stock > this.quantity
    ) {
      this.quantity += quantity
      this.emit('incremented')
    }
  }

  decrement(quantity = 1) {
    if (quantity > 0) this.quantity -= quantity
    this.emit('decremented')
  }
}

export default CartItem
