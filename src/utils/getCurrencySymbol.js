import commonCurrencies from '../assets/commonCurrencies.json'

function getCurrencySymbol(iso) {
  if (!(iso in commonCurrencies)) throw new Error('currency not found')
  return commonCurrencies[iso].symbol
}

export default getCurrencySymbol
