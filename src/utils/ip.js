import axios from 'axios'

const endpoints = {
  country: 'https://europe-west1-studio-moniker-shop-staging.cloudfunctions.net/getCustomerCountry',
}

export async function getCountry() {
  try {
    const res = await axios.get(endpoints.country)
    const d = {
      name: res.data.countryName,
      alpha2code: res.data.alpha2code
    }
    return d
  } catch (e) {
    console.error(e)
  }
}