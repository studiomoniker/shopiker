function formatPrice(locale, amount, currency) {
  const format = new Intl.NumberFormat(locale, { style: 'currency', currency, currencyDisplay: 'symbol' })
  const formatted = format.format(amount / 100)
  return formatted
}

export default formatPrice
