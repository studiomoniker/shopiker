import mitt from 'mitt'
import CartItem from './cartItem'
import Product from './product'
import Shipping from './shipping'
import ShippingMethod from './shippingMethod'
import formatPrice from './utils/formatPrice'

class Cart {
  constructor(options = {}) {
    this.locale = options.locale
    const mitter = mitt()
    this.on = mitter.on
    this.emit = mitter.emit
    this.off = mitter.off
    this.storage = options.cartStorage
    this.currency = options.currency
    this.standAlone = options.standAlone || false

    if (typeof window === 'undefined') console.warn('window not defined')
    if (typeof localStorage === 'undefined')
      console.warn('no localstorage available')

    const cart =
      typeof localStorage !== 'undefined' || !this.standAlone
        ? localStorage.getItem(this.storage)
        : undefined
    if (cart) {
      this.import(cart)
    } else {
      this.id = Math.random()
        .toString(16)
        .slice(2)
      this.createdAt = new Date().valueOf()
      this.updatedAt = new Date().valueOf()
      this.items = []
      this.shipping = new Shipping({
        items: this.items,
        locale: this.locale,
      })
    }
    this.shipping.on('updated', () => {
      this.cartUpdated()
    })
  }

  get itemsCount() {
    const count = this.items.reduce((acc, cur) => {
      acc += cur.quantity
      return acc
    }, 0)
    return count
  }

  get tax() {
    let tax = this.items.reduce((acc, cur) => {
      const { includesTax } = cur.product.price
      if (includesTax) {
        const single = cur.product.price.amount / (100 + cur.product.taxRate)
        const taxSingle = single * cur.product.taxRate
        const taxMulti = taxSingle * cur.quantity
        acc += taxMulti
      } else {
        const taxSingle = cur.product.price.amount * (cur.product.taxRate / 100)
        const taxMulti = taxSingle * cur.quantity
        acc += taxMulti
      }
      return acc
    }, 0)

    if (this.shipping.method) {
      const shippingAmountIncludesTax = this.shipping.method.includesTax
      const { taxRate } = this.shipping.method
      if (shippingAmountIncludesTax) {
        tax += (this.shipping.amount / (100 + taxRate)) * taxRate
      } else {
        tax += this.shipping.amount * taxRate
      }
    }

    const rounded = Math.round(tax)
    return rounded
  }

  get taxFormatted() {
    return formatPrice(this.locale, this.tax, this.currency)
  }

  get total() {
    const amountItems = this.items.reduce((acc, cur) => {
      acc += cur.amount
      return acc
    }, 0)

    const rounded = Math.round(amountItems + this.shipping.amount)
    return rounded
  }

  get totalFormatted() {
    return formatPrice(this.locale, this.total, this.currency)
  }

  get hasPhysical() {
    return this.items.reduce((acc, cur) => {
      if (cur.product.type === 'physical') acc = true
      return acc
    }, false)
  }

  get hasBackorders() {
    return this.items.reduce((acc, cur) => {
      if (cur.product.stock === 0 && cur.product.allowBackorder) acc = true
      return acc
    }, false)
  }

  getProductQuantity(product) {
    const item = this.items.find(item => item.id === product.id)
    if (item) {
      return item.quantity
    } else {
      return 0
    }
  }

  productIsInCart(product) {
    return this.items.findIndex(item => item.id === product.id) !== -1
  }

  addItem(product, quantity = 1) {
    if (!(product instanceof Product)) {
      product.locale = this.locale
      product = new Product(product)
    }

    if (!this.productIsInCart(product)) {
      if (!product.isInStock(quantity))
        throw new Error('Product is out in stock')

      const item = new CartItem({
        locale: this.locale,
        product,
        quantity,
      })
      this.items.push(item)

      this.attachItemListener(item)
    } else {
      const item = this.items.find(i => i.id === product.id)
      const combinedQuantity = item.quantity + quantity
      if (!product.isInStock(combinedQuantity))
        throw new Error('Product is out in stock')
      item.increment(quantity)
    }
    this.cartUpdated()
    this.emit('item-added', product.id)
  }

  attachItemListener(item) {
    item.on('incremented', () => {
      this.cartUpdated()
    })

    item.on('decremented', () => {
      if (item.quantity <= 0) {
        item.off('decremented')
        this.removeItem(item)
      }
      this.cartUpdated()
    })
  }

  removeItem(item) {
    const index = this.items.findIndex(i => i.id === item.id)
    this.items.splice(index, 1)
    this.cartUpdated()
    this.emit('item-removed')
  }

  export() {
    const cart = {
      ...this,
    }
    const exported = JSON.stringify(cart)
    return exported
  }

  save() {
    localStorage.setItem(this.storage, this.export())
  }

  import(string) {
    const json = JSON.parse(string)

    // RESTORE BASIC DATA
    this.id = json.id
    this.createdAt = json.createdAt
    this.updatedAt = json.updatedAt

    const now = Date.now()
    if (!this.standAlone && this.updatedAt < now - 24 * 60 * 60 * 1000) {
      this.clear()
    } else {
      // RESTORE ITEMS
      const items = json.items.map(item => {
        const cartItem = new CartItem({
          locale: this.locale,
          product: item.product,
          quantity: item.quantity,
        })

        this.attachItemListener(cartItem)

        return cartItem
      })
      this.items = items
    }

    // RESTORE SHIPPING
    if ('shipping' in json) {
      const method = new ShippingMethod(json.shipping.method)
      const { country } = json.shipping
      this.shipping = new Shipping({
        items: this.items,
        method,
        country,
        locale: this.locale,
      })
    } else {
      this.shipping = new Shipping({ items: this.items, locale: this.locale })
    }
  }

  cartUpdated({ save = true } = {}) {
    this.updatedAt = new Date().valueOf()
    if (save) {
      this.save()
    }
    this.emit('updated')
  }

  clear() {
    this.id = Math.random()
      .toString(16)
      .slice(2)
    this.createdAt = new Date().valueOf()
    this.updatedAt = new Date().valueOf()
    if (this.items) {
      this.items.splice(0, this.items.length)
    } else {
      this.items = []
    }
    this.save()
  }
}

export default Cart
