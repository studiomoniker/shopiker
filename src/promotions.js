import Promotion from './promotion'

class Promotions {
  constructor() {
    this.promotions = []
  }

  addPromotion(options) {
    const exists = this.promotions.find(promotion => promotion.key === options.key)
    if (exists) throw new Error('promotion already added')

    this.promotions.push(new Promotion(options))
  }
}

export default Promotions
