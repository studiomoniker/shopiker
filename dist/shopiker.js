(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("axios"), require("mitt"));
	else if(typeof define === 'function' && define.amd)
		define("shopiker", ["axios", "mitt"], factory);
	else if(typeof exports === 'object')
		exports["shopiker"] = factory(require("axios"), require("mitt"));
	else
		root["shopiker"] = factory(root["axios"], root["mitt"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_axios__, __WEBPACK_EXTERNAL_MODULE_mitt__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/asyncToGenerator.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/classCallCheck.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/createClass.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectSpread.js":
/*!*************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectSpread.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var defineProperty = __webpack_require__(/*! ./defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    var ownKeys = Object.keys(source);

    if (typeof Object.getOwnPropertySymbols === 'function') {
      ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
        return Object.getOwnPropertyDescriptor(source, sym).enumerable;
      }));
    }

    ownKeys.forEach(function (key) {
      defineProperty(target, key, source[key]);
    });
  }

  return target;
}

module.exports = _objectSpread;

/***/ }),

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./src/cart.js":
/*!*********************!*\
  !*** ./src/cart.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ "./node_modules/@babel/runtime/helpers/objectSpread.js");
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mitt */ "mitt");
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mitt__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _cartItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cartItem */ "./src/cartItem.js");
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product */ "./src/product.js");
/* harmony import */ var _shipping__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./shipping */ "./src/shipping.js");
/* harmony import */ var _shippingMethod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shippingMethod */ "./src/shippingMethod.js");
/* harmony import */ var _utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utils/formatPrice */ "./src/utils/formatPrice.js");










var Cart =
/*#__PURE__*/
function () {
  function Cart() {
    var _this = this;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Cart);

    this.locale = options.locale;
    var mitter = mitt__WEBPACK_IMPORTED_MODULE_3___default()();
    this.on = mitter.on;
    this.emit = mitter.emit;
    this.off = mitter.off;
    this.storage = options.cartStorage;
    this.currency = options.currency;
    this.standAlone = options.standAlone || false;
    if (typeof window === 'undefined') console.warn('window not defined');
    if (typeof localStorage === 'undefined') console.warn('no localstorage available');
    var cart = typeof localStorage !== 'undefined' || !this.standAlone ? localStorage.getItem(this.storage) : undefined;

    if (cart) {
      this["import"](cart);
    } else {
      this.id = Math.random().toString(16).slice(2);
      this.createdAt = new Date().valueOf();
      this.updatedAt = new Date().valueOf();
      this.items = [];
      this.shipping = new _shipping__WEBPACK_IMPORTED_MODULE_6__["default"]({
        items: this.items,
        locale: this.locale
      });
    }

    this.shipping.on('updated', function () {
      _this.cartUpdated();
    });
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Cart, [{
    key: "getProductQuantity",
    value: function getProductQuantity(product) {
      var item = this.items.find(function (item) {
        return item.id === product.id;
      });

      if (item) {
        return item.quantity;
      } else {
        return 0;
      }
    }
  }, {
    key: "productIsInCart",
    value: function productIsInCart(product) {
      return this.items.findIndex(function (item) {
        return item.id === product.id;
      }) !== -1;
    }
  }, {
    key: "addItem",
    value: function addItem(product) {
      var quantity = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

      if (!(product instanceof _product__WEBPACK_IMPORTED_MODULE_5__["default"])) {
        product.locale = this.locale;
        product = new _product__WEBPACK_IMPORTED_MODULE_5__["default"](product);
      }

      if (!this.productIsInCart(product)) {
        if (!product.isInStock(quantity)) throw new Error('Product is out in stock');
        var item = new _cartItem__WEBPACK_IMPORTED_MODULE_4__["default"]({
          locale: this.locale,
          product: product,
          quantity: quantity
        });
        this.items.push(item);
        this.attachItemListener(item);
      } else {
        var _item = this.items.find(function (i) {
          return i.id === product.id;
        });

        var combinedQuantity = _item.quantity + quantity;
        if (!product.isInStock(combinedQuantity)) throw new Error('Product is out in stock');

        _item.increment(quantity);
      }

      this.cartUpdated();
      this.emit('item-added', product.id);
    }
  }, {
    key: "attachItemListener",
    value: function attachItemListener(item) {
      var _this2 = this;

      item.on('incremented', function () {
        _this2.cartUpdated();
      });
      item.on('decremented', function () {
        if (item.quantity <= 0) {
          item.off('decremented');

          _this2.removeItem(item);
        }

        _this2.cartUpdated();
      });
    }
  }, {
    key: "removeItem",
    value: function removeItem(item) {
      var index = this.items.findIndex(function (i) {
        return i.id === item.id;
      });
      this.items.splice(index, 1);
      this.cartUpdated();
      this.emit('item-removed');
    }
  }, {
    key: "export",
    value: function _export() {
      var cart = _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default()({}, this);

      var exported = JSON.stringify(cart);
      return exported;
    }
  }, {
    key: "save",
    value: function save() {
      localStorage.setItem(this.storage, this["export"]());
    }
  }, {
    key: "import",
    value: function _import(string) {
      var _this3 = this;

      var json = JSON.parse(string); // RESTORE BASIC DATA

      this.id = json.id;
      this.createdAt = json.createdAt;
      this.updatedAt = json.updatedAt;
      var now = Date.now();

      if (!this.standAlone && this.updatedAt < now - 24 * 60 * 60 * 1000) {
        this.clear();
      } else {
        // RESTORE ITEMS
        var items = json.items.map(function (item) {
          var cartItem = new _cartItem__WEBPACK_IMPORTED_MODULE_4__["default"]({
            locale: _this3.locale,
            product: item.product,
            quantity: item.quantity
          });

          _this3.attachItemListener(cartItem);

          return cartItem;
        });
        this.items = items;
      } // RESTORE SHIPPING


      if ('shipping' in json) {
        var method = new _shippingMethod__WEBPACK_IMPORTED_MODULE_7__["default"](json.shipping.method);
        var country = json.shipping.country;
        this.shipping = new _shipping__WEBPACK_IMPORTED_MODULE_6__["default"]({
          items: this.items,
          method: method,
          country: country,
          locale: this.locale
        });
      } else {
        this.shipping = new _shipping__WEBPACK_IMPORTED_MODULE_6__["default"]({
          items: this.items,
          locale: this.locale
        });
      }
    }
  }, {
    key: "cartUpdated",
    value: function cartUpdated() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$save = _ref.save,
          save = _ref$save === void 0 ? true : _ref$save;

      this.updatedAt = new Date().valueOf();

      if (save) {
        this.save();
      }

      this.emit('updated');
    }
  }, {
    key: "clear",
    value: function clear() {
      this.id = Math.random().toString(16).slice(2);
      this.createdAt = new Date().valueOf();
      this.updatedAt = new Date().valueOf();

      if (this.items) {
        this.items.splice(0, this.items.length);
      } else {
        this.items = [];
      }

      this.save();
    }
  }, {
    key: "itemsCount",
    get: function get() {
      var count = this.items.reduce(function (acc, cur) {
        acc += cur.quantity;
        return acc;
      }, 0);
      return count;
    }
  }, {
    key: "tax",
    get: function get() {
      var tax = this.items.reduce(function (acc, cur) {
        var includesTax = cur.product.price.includesTax;

        if (includesTax) {
          var single = cur.product.price.amount / (100 + cur.product.taxRate);
          var taxSingle = single * cur.product.taxRate;
          var taxMulti = taxSingle * cur.quantity;
          acc += taxMulti;
        } else {
          var _taxSingle = cur.product.price.amount * (cur.product.taxRate / 100);

          var _taxMulti = _taxSingle * cur.quantity;

          acc += _taxMulti;
        }

        return acc;
      }, 0);

      if (this.shipping.method) {
        var shippingAmountIncludesTax = this.shipping.method.includesTax;
        var taxRate = this.shipping.method.taxRate;

        if (shippingAmountIncludesTax) {
          tax += this.shipping.amount / (100 + taxRate) * taxRate;
        } else {
          tax += this.shipping.amount * taxRate;
        }
      }

      var rounded = Math.round(tax);
      return rounded;
    }
  }, {
    key: "taxFormatted",
    get: function get() {
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__["default"])(this.locale, this.tax, this.currency);
    }
  }, {
    key: "total",
    get: function get() {
      var amountItems = this.items.reduce(function (acc, cur) {
        acc += cur.amount;
        return acc;
      }, 0);
      var rounded = Math.round(amountItems + this.shipping.amount);
      return rounded;
    }
  }, {
    key: "totalFormatted",
    get: function get() {
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__["default"])(this.locale, this.total, this.currency);
    }
  }, {
    key: "hasPhysical",
    get: function get() {
      return this.items.reduce(function (acc, cur) {
        if (cur.product.type === 'physical') acc = true;
        return acc;
      }, false);
    }
  }, {
    key: "hasBackorders",
    get: function get() {
      return this.items.reduce(function (acc, cur) {
        if (cur.product.stock === 0 && cur.product.allowBackorder) acc = true;
        return acc;
      }, false);
    }
  }]);

  return Cart;
}();

/* harmony default export */ __webpack_exports__["default"] = (Cart);

/***/ }),

/***/ "./src/cartItem.js":
/*!*************************!*\
  !*** ./src/cartItem.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product */ "./src/product.js");
/* harmony import */ var _utils_formatPrice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/formatPrice */ "./src/utils/formatPrice.js");
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! mitt */ "mitt");
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(mitt__WEBPACK_IMPORTED_MODULE_4__);






var CartItem =
/*#__PURE__*/
function () {
  function CartItem() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        locale = _ref.locale,
        _ref$quantity = _ref.quantity,
        quantity = _ref$quantity === void 0 ? 0 : _ref$quantity,
        _ref$product = _ref.product,
        product = _ref$product === void 0 ? undefined : _ref$product;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, CartItem);

    this.product = product instanceof _product__WEBPACK_IMPORTED_MODULE_2__["default"] ? product : new _product__WEBPACK_IMPORTED_MODULE_2__["default"](product);
    this.locale = locale;
    this.id = product.id;
    this.quantity = quantity;
    var mitter = mitt__WEBPACK_IMPORTED_MODULE_4___default()();
    this.on = mitter.on;
    this.off = mitter.off;
    this.emit = mitter.emit;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(CartItem, [{
    key: "increment",
    value: function increment() {
      var quantity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

      if (quantity < 0) {
        this.decrement(-quantity);
      } else if (!this.product.manageStock || this.product.allowBackorder || this.product.stock > this.quantity) {
        this.quantity += quantity;
        this.emit('incremented');
      }
    }
  }, {
    key: "decrement",
    value: function decrement() {
      var quantity = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      if (quantity > 0) this.quantity -= quantity;
      this.emit('decremented');
    }
  }, {
    key: "amount",
    get: function get() {
      return this.product.amount * this.quantity;
    }
  }, {
    key: "amountFormatted",
    get: function get() {
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_3__["default"])(this.locale, this.amount, this.product.price.currency);
    }
  }]);

  return CartItem;
}();

/* harmony default export */ __webpack_exports__["default"] = (CartItem);

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./store */ "./src/store.js");
/* harmony import */ var _order__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./order */ "./src/order.js");
/* harmony import */ var _cart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cart */ "./src/cart.js");



var shopiker = {
  Store: _store__WEBPACK_IMPORTED_MODULE_0__["default"],
  Order: _order__WEBPACK_IMPORTED_MODULE_1__["default"],
  Cart: _cart__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (shopiker);

/***/ }),

/***/ "./src/order.js":
/*!**********************!*\
  !*** ./src/order.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! mitt */ "mitt");
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mitt__WEBPACK_IMPORTED_MODULE_2__);




var Order =
/*#__PURE__*/
function () {
  function Order(_ref) {
    var cart = _ref.cart,
        customerId = _ref.customerId,
        _ref$orderStorage = _ref.orderStorage,
        orderStorage = _ref$orderStorage === void 0 ? 'order' : _ref$orderStorage,
        _ref$addressesStorage = _ref.addressesStorage,
        addressesStorage = _ref$addressesStorage === void 0 ? 'addresses' : _ref$addressesStorage;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Order);

    if (orderStorage && localStorage.getItem(orderStorage)) {
      var orderData = JSON.parse(localStorage.getItem(orderStorage));
      this.id = orderData.id;
      this.customerId = orderData.customerId;
      this.createdAt = orderData.createdAt;
      this.updatedAt = orderData.updatedAt;
    } else {
      // this.id = Math.random().toString(16).slice(2)
      this.customerId = customerId;
      this.createdAt = new Date().valueOf();
      this.updatedAt = new Date().valueOf();
    }

    if (addressesStorage && sessionStorage.getItem(addressesStorage)) {
      var addressesData = JSON.parse(sessionStorage.getItem(addressesStorage));

      if ('billing' in addressesData) {
        this.billing = addressesData.billing;
      }

      if ('shipping' in addressesData) {
        this.shipping = addressesData.shipping;
      }
    } else {
      this.billing = undefined;
      this.shipping = undefined;
    }

    this.orderStorage = orderStorage;
    this.addressesStorage = addressesStorage;
    this.cart = cart;
    var mitter = mitt__WEBPACK_IMPORTED_MODULE_2___default()();
    this.on = mitter.on;
    this.off = mitter.off;
    this.emit = mitter.emit;
    this.save();
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Order, [{
    key: "toJson",
    value: function toJson() {
      var order = JSON.parse(JSON.stringify(this));
      order.cart.shipping.amount = parseInt(this.cart.shipping.amount, 10);
      order.cart.shipping.needsShipping = this.cart.shipping.needsShipping;
      order.cart.shipping.amountFormatted = this.cart.shipping.amountFormatted;
      order.cart.shipping.zone = this.cart.shipping.zone;
      order.cart.items = this.cart.items.map(function (item) {
        var i = JSON.parse(JSON.stringify(item));
        i.amount = parseInt(item.amount, 10);
        i.amountFormatted = item.amountFormatted;
        i.product.isVariation = item.product.isVariation;
        i.product.amount = parseInt(item.product.amount, 10);
        i.product.amountFormatted = item.product.amountFormatted;
        return i;
      });
      order.separateShippingAddress = this.separateShippingAddress;
      order.hasAddress = this.hasAddress;
      return order;
    }
  }, {
    key: "save",
    value: function save() {
      this.emit('updated');
      this.updatedAt = new Date().valueOf();
      var d = {
        id: this.id,
        customerId: this.customerId,
        createdAt: this.createdAt,
        updatedAt: this.updatedAt
      };
      localStorage.setItem(this.orderStorage, JSON.stringify(d));
      var a = {
        billing: this.billing,
        shipping: this.shipping
      };
      sessionStorage.setItem(this.addressesStorage, JSON.stringify(a));
    }
  }, {
    key: "setAddresses",
    value: function setAddresses(_ref2) {
      var billing = _ref2.billing,
          shipping = _ref2.shipping;
      this.shipping = shipping;
      this.billing = billing;
      this.assignShippingAddress();
      this.save();
    }
  }, {
    key: "assignShippingAddress",
    value: function assignShippingAddress() {
      if (this.shipping && this.separateShippingAddress && 'country' in this.shipping && 'alpha2code' in this.shipping.country && this.shipping.country.alpha2code.length === 2) {
        this.cart.shipping.setCountry(this.shipping.country);
      } else if (this.separateShippingAddress) {
        this.cart.shipping.setCountryByIp();
      } else if (!this.separateShippingAddress && this.billing && 'country' in this.billing && 'alpha2code' in this.billing.country && this.billing.country.alpha2code.length === 2) {
        this.cart.shipping.setCountry(this.billing.country);
      } else {
        this.cart.shipping.setCountryByIp();
      }
    }
  }, {
    key: "clear",
    value: function clear() {
      // this.id = Math.random().toString(16).slice(2)
      this.id = undefined;
      this.createdAt = new Date().valueOf();
      this.updatedAt = new Date().valueOf();
      this.customerId = undefined;
      this.billing = undefined;
      this.shipping = undefined;
      this.save();
    }
  }, {
    key: "clearCustomerId",
    value: function clearCustomerId() {
      this.customerId = undefined;
      this.save();
    }
  }, {
    key: "setCustomerId",
    value: function setCustomerId(id) {
      this.customerId = id;
      this.save();
    }
  }, {
    key: "separateShippingAddress",
    get: function get() {
      return !!this.shipping;
    }
  }, {
    key: "hasAddress",
    get: function get() {
      return !!this.billing || !!this.shipping;
    }
  }]);

  return Order;
}();

/* harmony default export */ __webpack_exports__["default"] = (Order);

/***/ }),

/***/ "./src/product.js":
/*!************************!*\
  !*** ./src/product.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_formatPrice__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/formatPrice */ "./src/utils/formatPrice.js");




var Product =
/*#__PURE__*/
function () {
  function Product(options) {
    var _this = this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Product);

    if (options.id === undefined) throw new Error('id is undefined');
    if (options.title === undefined) throw new Error('title is undefined');
    if (options.subtitle === undefined) throw new Error('subtitle is undefined');
    if (options.taxRate === undefined) throw new Error('taxRate is undefined');
    if (options.stock === undefined) throw new Error('stock is undefined');
    if (options.manageStock === undefined) throw new Error('manageStock is undefined');
    if (options.allowBackorder === undefined) throw new Error('allowBackorder is undefined');
    if (options.status === undefined) throw new Error('status is undefined');
    if (options.type === undefined) throw new Error('type is undefined');
    if (options.description === undefined) throw new Error('description is undefined');
    if (options.price === undefined) throw new Error('price is undefined');
    if (!('amount' in options.price) || !('currency' in options.price) || !('includesTax' in options.price)) throw new Error('Price object is not valid');
    this.locale = options.locale;
    this.id = options.id;
    this.title = options.title;
    this.subtitle = options.subtitle;
    this.taxRate = options.taxRate;
    this.stock = options.stock;
    this.manageStock = options.manageStock;
    this.allowBackorder = options.allowBackorder;
    this.status = options.status;
    this.type = options.type;
    this.description = options.description;
    this.price = options.price;
    this.parent = options.parent || undefined; // ADD META DATA, EG EVERYTHING EXCEPT REQUIRED DATA

    if ('meta' in options) this.meta = options.meta;
    var productKeys = ['amount', 'amountFormatted', 'isVariation', 'locale', 'parent', 'id', 'title', 'subtitle', 'taxRate', 'stock', 'manageStock', 'allowBackorder', 'status', 'type', 'images', 'description', 'price'];
    Object.keys(options).forEach(function (key) {
      if (key === 'meta' || productKeys.includes(key)) return;
      if (!('meta' in _this)) _this.meta = {};
      _this.meta[key] = options[key];
    });
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Product, [{
    key: "isInStock",
    value: function isInStock(quantity) {
      if (!this.manageStock || this.allowBackorder) return true;
      if (this.stock >= quantity) return true;
      return false;
    }
  }, {
    key: "isVariation",
    get: function get() {
      return !!this.parent;
    }
  }, {
    key: "amount",
    get: function get() {
      if (this.price.includesTax) {
        return this.price.amount;
      }

      return this.price.amount * (1 + this.taxRate / 100);
    }
  }, {
    key: "amountFormatted",
    get: function get() {
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_2__["default"])(this.locale, this.amount, this.price.currency);
    }
  }]);

  return Product;
}();

/* harmony default export */ __webpack_exports__["default"] = (Product);

/***/ }),

/***/ "./src/shipping.js":
/*!*************************!*\
  !*** ./src/shipping.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/objectSpread */ "./node_modules/@babel/runtime/helpers/objectSpread.js");
/* harmony import */ var _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! mitt */ "mitt");
/* harmony import */ var mitt__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(mitt__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _utils_ip__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils/ip */ "./src/utils/ip.js");
/* harmony import */ var _shippingMethod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shippingMethod */ "./src/shippingMethod.js");
/* harmony import */ var _utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./utils/formatPrice */ "./src/utils/formatPrice.js");






/* eslint-disable no-console */





var Shipping =
/*#__PURE__*/
function () {
  function Shipping() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$method = _ref.method,
        method = _ref$method === void 0 ? undefined : _ref$method,
        _ref$country = _ref.country,
        country = _ref$country === void 0 ? {
      alpha2code: '',
      name: ''
    } : _ref$country,
        items = _ref.items,
        locale = _ref.locale;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Shipping);

    if (method && !(method instanceof _shippingMethod__WEBPACK_IMPORTED_MODULE_7__["default"])) method = new _shippingMethod__WEBPACK_IMPORTED_MODULE_7__["default"](method);
    this.locale = locale;
    this.items = items;
    this.country = country;
    this.method = method;
    var mitter = mitt__WEBPACK_IMPORTED_MODULE_5___default()();
    this.on = mitter.on;
    this.emit = mitter.emit;
    this.off = mitter.off;

    if (this.country.alpha2code === '' || this.country.name === '') {
      this.setCountryByIp();
    }
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Shipping, [{
    key: "setCountry",
    value: function setCountry(country) {
      if (this.country.alpha2code !== country.alpha2code) {
        this.country = country;
        this.emit('country-changed');
        this.emit('updated');
      } else {
        console.warn('did not update shipping country because country is already set');
      }
    }
  }, {
    key: "setMethod",
    value: function setMethod(_ref2) {
      var method = _ref2.method;

      if (this.method) {
        console.warn('did not update shipping method because method is already set');
        return;
      }

      this.method = method;
      this.emit('method-changed');
      this.emit('updated');
    }
  }, {
    key: "setCountryByIp",
    value: function () {
      var _setCountryByIp = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return Object(_utils_ip__WEBPACK_IMPORTED_MODULE_6__["getCountry"])();

              case 2:
                this.country = _context.sent;

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function setCountryByIp() {
        return _setCountryByIp.apply(this, arguments);
      }

      return setCountryByIp;
    }()
  }, {
    key: "amount",
    get: function get() {
      if (!this.zone || this.items.length === 0 || !this.needsShipping) return 0;

      if (this.method.includesTax) {
        return this.zone.amount;
      }

      var amount = this.zone.amount * (1 + this.method.taxRate / 100);
      return amount;
    }
  }, {
    key: "needsShipping",
    get: function get() {
      return this.items.reduce(function (acc, cur) {
        if (cur.product.type === 'physical') acc = true;
        return acc;
      }, false);
    }
  }, {
    key: "amountFormatted",
    get: function get() {
      if (!this.method || !this.zone || !this.needsShipping) return '-';
      var currency = this.method.currency;
      if (this.amount === 0 || this.items.length === 0) return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__["default"])(this.locale, 0, currency);
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_8__["default"])(this.locale, this.amount, currency);
    }
  }, {
    key: "zone",
    get: function get() {
      var _this = this;

      if (!this.method) return undefined;

      if (this.country.alpha2code !== '' && this.country.name !== '') {
        var _zone = this.method.zones.find(function (z) {
          if (z.countries.includes(_this.country.alpha2code)) return true;
          return false;
        });

        if (_zone) return _zone;
      }

      var zone = this.method.zones.reduce(function (acc, cur) {
        if (cur.amount > acc.amount) {
          acc = cur;
        }

        return acc;
      }, _babel_runtime_helpers_objectSpread__WEBPACK_IMPORTED_MODULE_0___default()({}, this.method.zones[0]));
      return zone;
    }
  }]);

  return Shipping;
}();

/* harmony default export */ __webpack_exports__["default"] = (Shipping);

/***/ }),

/***/ "./src/shippingMethod.js":
/*!*******************************!*\
  !*** ./src/shippingMethod.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);


var ShippingMethod = function ShippingMethod(options) {
  _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, ShippingMethod);

  this.id = options.id || 'standard';
  this.isDefault = options.isDefault;
  this.name = options.name;
  this.zones = options.zones;
  this.includesTax = options.includesTax;
  this.currency = options.currency;
  this.description = options.description;
  this.taxRate = options.taxRate;
};

/* harmony default export */ __webpack_exports__["default"] = (ShippingMethod);

/***/ }),

/***/ "./src/store.js":
/*!**********************!*\
  !*** ./src/store.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _order__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./order */ "./src/order.js");
/* harmony import */ var _cart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cart */ "./src/cart.js");
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product */ "./src/product.js");
/* harmony import */ var _shippingMethod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shippingMethod */ "./src/shippingMethod.js");
/* harmony import */ var _utils_formatPrice__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils/formatPrice */ "./src/utils/formatPrice.js");








var Store =
/*#__PURE__*/
function () {
  function Store() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$cartStorage = _ref.cartStorage,
        cartStorage = _ref$cartStorage === void 0 ? 'cart' : _ref$cartStorage,
        _ref$orderStorage = _ref.orderStorage,
        orderStorage = _ref$orderStorage === void 0 ? 'order' : _ref$orderStorage,
        _ref$addressesStorage = _ref.addressesStorage,
        addressesStorage = _ref$addressesStorage === void 0 ? 'addresses' : _ref$addressesStorage,
        _ref$currency = _ref.currency,
        currency = _ref$currency === void 0 ? 'EUR' : _ref$currency,
        _ref$locale = _ref.locale,
        locale = _ref$locale === void 0 ? 'en-US' : _ref$locale;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, Store);

    this.currency = currency;
    this.locale = locale;
    this.cart = new _cart__WEBPACK_IMPORTED_MODULE_3__["default"]({
      cartStorage: cartStorage,
      currency: currency,
      locale: locale
    });
    this.order = new _order__WEBPACK_IMPORTED_MODULE_2__["default"]({
      cart: this.cart,
      orderStorage: orderStorage,
      addressesStorage: addressesStorage
    });
    this.shippingMethods = [];
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(Store, [{
    key: "setShippingMethods",
    value: function setShippingMethods(shippingMethods) {
      this.shippingMethods = shippingMethods.map(function (data) {
        return new _shippingMethod__WEBPACK_IMPORTED_MODULE_5__["default"](data);
      });
      this.setDefaultShippingMethod();
    }
  }, {
    key: "setDefaultShippingMethod",
    value: function setDefaultShippingMethod() {
      var method = this.shippingMethods.find(function (shippingMethod) {
        return shippingMethod.isDefault;
      });

      if (method) {
        this.cart.shipping.setMethod({
          method: method
        });
      }
    }
  }, {
    key: "createProduct",
    value: function createProduct(data) {
      data.locale = this.locale;
      return new _product__WEBPACK_IMPORTED_MODULE_4__["default"](data);
    }
  }, {
    key: "clear",
    value: function clear() {
      this.cart.clear();
      this.order.clear();
      this.setDefaultShippingMethod();
      this.cart.shipping.setCountryByIp();
    }
  }, {
    key: "formatPrice",
    value: function formatPrice(amount) {
      return Object(_utils_formatPrice__WEBPACK_IMPORTED_MODULE_6__["default"])(this.locale, amount, this.currency);
    }
  }]);

  return Store;
}();

/* harmony default export */ __webpack_exports__["default"] = (Store);

/***/ }),

/***/ "./src/utils/formatPrice.js":
/*!**********************************!*\
  !*** ./src/utils/formatPrice.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function formatPrice(locale, amount, currency) {
  var format = new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency,
    currencyDisplay: 'symbol'
  });
  var formatted = format.format(amount / 100);
  return formatted;
}

/* harmony default export */ __webpack_exports__["default"] = (formatPrice);

/***/ }),

/***/ "./src/utils/ip.js":
/*!*************************!*\
  !*** ./src/utils/ip.js ***!
  \*************************/
/*! exports provided: getCountry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCountry", function() { return getCountry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);



var endpoints = {
  country: 'https://europe-west1-studio-moniker-shop-staging.cloudfunctions.net/getCustomerCountry'
};
function getCountry() {
  return _getCountry.apply(this, arguments);
}

function _getCountry() {
  _getCountry = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var res, d;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(endpoints.country);

          case 3:
            res = _context.sent;
            d = {
              name: res.data.countryName,
              alpha2code: res.data.alpha2code
            };
            return _context.abrupt("return", d);

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);
            console.error(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 8]]);
  }));
  return _getCountry.apply(this, arguments);
}

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_axios__;

/***/ }),

/***/ "mitt":
/*!***********************!*\
  !*** external "mitt" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_mitt__;

/***/ })

/******/ });
});
//# sourceMappingURL=shopiker.js.map