# Shopiker

Shopiker is a client and server side JavaScript library for handling common e-commerce things. Add items to the cart, remove items, calculate the total amount and including tax, choosing shipping methods and setting shipping zones.

## Development

For now it's best to develop this project in conjuction with the project it is made for: [Moniker Shop](https://bitbucket.org/studiomoniker/shop-frontend). First checkout this repository on run the following from that directory:

``` bash
npm link
npm run dev
```

The first command *links* this package folder to the global `node_ modules` folder. See [npm's documentation](https://docs.npmjs.com/cli/link.html) for more information. The second commands starts a development server that will automatically rebuild the package whenever you make a change.

To use the development version on the frontend you should `cd` to the frontend's project folder and run:

``` bash
npm link shopiker
```

You might want to restart your development server for the frontend to make sure it loads the right package. When running the development server for this package it should automatically update the frontend whenever you make a change. This might be a bit buggy though, so just restart both servers in case something is not working.

## Deployment

Whenever you're done developing this package you need to create a new build. Run `npm run build` and commit the changes to the `dist` directory. Make sure to rebuild the shop frontend to use the latest version of shopiker.

## Installation

Not yet :)

## Usage

```javascript
import shopiker from 'shopiker'

const store = new shopiker.Store({
  currency: 'EUR'
})

const product = {
  allowBackorder: false
  description: 'This is the product description'
  id: 'do-not-draw-a-penis-kitchen-towel'
  images: []
  manageStock: true
  price: {
    amount: 1700
    currency: 'EUR'
    includesTax: true
  },
  sku: 'do-not-draw-a-penis-kitchen-towel-1'
  status: true
  stock: 400
  subtitle: 'Kitchen Towel'
  taxRate: 21
  title: 'Do Not Draw a Penis'
  type: 'physical'
}

store.cart.addItem(product, 1)

console.log(store.cart.total)
```

---

### Store
```javascript
const store = new shopiker.Store(options)
```
Used to initialize store

#### Options
```javascript
{
  cartStorage: 'cart',
  // key for storing the cart on the clients localStorage

  orderStorage: 'order',
  // key for storing the order on the clients localStorage

  addressesStorage: 'addresses',
  // key for storing the addresses on the clients sessionStorage

  currency: 'EUR',
  // main currency used for price calculation and number formatting

  locale: 'en-US'
  // locale used for number formatting
}
```

#### Properties
`currency` - String - three digit uppercase String ('EUR')

`cart` - Instance of class Cart

`order` - Instance of class Order

`shippingMethods` - Array - all available shipping methods, should be set as soon as finished retrieving from server by calling `setShippingMethods()`

`locale` - String - used for number formatting, inherited to cart


#### Methods

```javascript
setShippingMethods(shippingMethods)
// (Array)shippingMethods
// example:
// [
//   {
//     currency: 'EUR',
//     description: 'Standard shipping method',
//     id: 'standard',
//     includesTax: true,
//     isDefault: true,
//     name: 'standard',
//     taxRate: 21,
//     zones: [
//       {
//         amount: 500,
//         countries: [ 'NL' ],
//         id: '0',
//         name: 'Netherlands',
//       },
//       {
//         amount: 1000,
//         countries: [ 'DE', 'US' ],
//         id: '1',
//         name: 'International',
//       }
//     ]
//   },
//   {...}
// ]

setDefaultShippingMethod()
// find shipping method which isDefault, set as shipping method

createProduct(productData)
// (Object)productData
// example:
// {
//   allowBackorder: false
//   description: 'This is the product description'
//   id: 'do-not-draw-a-penis-kitchen-towel'
//   images: []
//   manageStock: true
//   price: {
//     amount: 1700
//     currency: 'EUR'
//     includesTax: true
//   },
//   sku: 'do-not-draw-a-penis-kitchen-towel-1'
//   status: true
//   stock: 400
//   subtitle: 'Kitchen Towel'
//   taxRate: 21
//   title: 'Do Not Draw a Penis'
//   type: 'physical'
// }

clear()
// clears cart and order, sessionstorage and 
// localstorage items, resets to default shipping 
// method and sets shipping country to country
// retrieved by IP
```

---

### Cart
```javascript
const cart = store.cart
```

#### Properties
`currency` - String - three digit uppercase String ('EUR'), inherited from `Store`

`id` - String - automatically generated id
`createdAt` - Int - unix timestamp in milliseconds, , is reset on calling store.clear(), saved on and loaded from localStorage

`updatedAt` - Int - unix timestamp in milliseconds, , is reset on calling store.clear(), saved on and loaded from localStorage

`items` - Array - Array of CartItems

`shipping` - Instance of [Shipping](#shipping) - current shipping method

`storage` - String - key for saving the cart to localStorage, defaults to 'cart'

`locale` - String - inherited from `Store`, used for number formatting, inherited to cart

Getter `itemsCount` - Int - count of cartItems

Getter `tax` - Int - amount of tax in `currency`

Getter `taxFormatted` - String - tax formatted in `currency`

Getter `total` - Int - total amount in `currency`

Getter `totalFormatted` - String - total formatted in `currency`

Getter `hasPhysical` - Boolean - cart holds physical products, used to calculate shipping costs


#### Methods

```javascript
productIsInCart(product)
// (Object || Product Instance)product - see example @ Usage
// returns is Product is already in Cart

addItem(product, quantity)
// (Object || Product Instance)product - see example @ Usage
// (Int)quantity
// Adds a product to the cart

removeItem(product, quantity)
// (Object || Product Instance)product - see example @ Usage
// (Int)quantity
// removes a cart item from cart

clear()
// Called from store.clear()
```

#### Events

```javascript
cart.on('updated', () => {
  console.log('cart updated!')
})
```

`updated` - item added or removed from cart or shipping changed (has influence on amounts)

`item-added` - item is added to the cart

`item-removed` - item is removed from the cart

---

### Order
```javascript
const cart = store.order
```

#### Properties
`id` - String - the id of the order, is reset on calling store.clear(), saved on and loaded from localStorage

`createdAt` - Int - unix timestamp in milliseconds, , is reset on calling store.clear(), saved on and loaded from localStorage

`updatedAt` - Int - unix timestamp in milliseconds, , is reset on calling store.clear(), saved on and loaded from localStorage

`customerId` - String - customer ID, saved on and loaded from localStorage

`billing` - Object - saved on and loaded from sessionStorage

`shipping` - Object - saved on and loaded from sessionStorage

Getter `separateShippingAddress` - Boolean - if true, billing and shipping address are separate


#### Methods

```javascript
setShippingAddress({ address })
// sets the shipping address

clearShippingAddress()
// clears the shipping address

setBillingAddress({ address })
// sets the billing address

clearBillingAddress()
// clears the billing address

clear()
// clears the order, called on store.clear()

clearCustomerId()
// clears the customerId

setCustomerId(id)
// (String)id
// sets the customerId
```

#### Events

```javascript
order.on('updated', () => {
  console.log('order updated!')
})
```

`updated` - every change to order

---

### Shipping
```javascript
const shipping = store.cart.shipping
```
The currently selected shipping method

#### Properties
`locale` - String - inherited from [Cart](#cart), used for number formatting

`items` - Array - Reference to cart.items in [Cart](#cart), used to calculate shipping

`country` - Object - i.e. `{ alpha2code: 'DE', name: 'Germany' }`, set by order addresses, used to determine the shipping zone, useful for showing shipping destination

`method` - Instance of [ShippingMethod](#shipping-method) - currently selected shipping method

Getter `amount` - Int - amount of Shipping incl. Tax

Getter `amountFormatted` - String - amount formatted in `currency`

Getter `zone` - Object - Current Shipping zone based on method and country


#### Methods

```javascript
setCountry(country)
// country - Object - i.e. `{ alpha2code: 'DE', name: 'Germany' }`
// sets the country to ship to

setMethod({ method })
// method - Instance of ShippingMethod]
// set Shipping method, has to be instance of class ShippingMethod
```

#### Events

```javascript
shipping.on('updated', () => {
  console.log('shipping updated!')
})
```

`updated` - shipping method or destination country changed

`method-changed` - shipping method changed

`country-changed` - destination country changed

---

## shopiker-admin

shopiker is acompanied by shopiker-admin, a node library for validating shopiker orders. Pass the order to shopiker-admin and validate orders on server side like this:

client.js
```javascript
// customer is checking out
const order = store.order
```

server.js
```javascript
const order = new admin.Order(orderDataFromClient)

order.validate({
  validate: (cartItem) => {
    return true // every order is valid
  }
})
```